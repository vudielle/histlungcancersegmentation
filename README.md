# README #

## Contents ##

- About
- Licensing statement and third party libraries
- Usage  
-- Command line usage  
-- ImageJ plugin  
-- Notes on using your own visual dictionary  
- Contact


## About ##

IHC Lung Cancer Segmentation is a java application to segment pan-cytokeratin stained histological images of non-small cell lung carcinoma, using information computed from a visual dictionary to process images.  
It is based on ImageJ, an open-source image processing software, https://imagej.nih.gov/ij/  
It was developed at the Swiss Federal Institute of Technology (ETH) Zurich and University Hospital Zurich as a submission to the ISBI 2016 - http://biomedicalimaging.org/2016/  


## Licensing statement and third party libraries ##

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses/

Besides the basic ImageJ library, the application uses the following two libraries, both also published under the GNU GPL 3:  
 CMP-BIA tools - http://imagej.net/CMP-BIA_tools  
 IJBlob - http://imagej.net/IJ_Blob  


## Usage ##

The application can be used as a command line tool or as a plugin in ImageJ.  
It was developed to segment histology cores at 40x magnification, i.e. images of size 3100x3100 pixels, with 1pixel equal to 0.23mum.  
The segmentation of an image of this size will take about 2-3 minutes.  
It should work on bigger images (obviously increasing execution time), but this has not been tested.
However, it will not work properly at any other magnification.

### 1 Command line usage ###

Download and unpack IhcLungCancerSeg.zip from the download page.
Open a command window in the folder containing Ihc_Lung_Cancer_Seg.jar and use the following command to start the application:  
java -jar Ihc_Lung_Cancer_Seg.jar  
Follow the instructions to provide a source file or folder. If a folder is entered, the application will process any *.jpg files within said folder.  
Furthermore you can choose to use your own visual dictionary, or the provided data. See notes at the end of this section concerning using your own dictionary.  
The resulting image(s) will be saved in the same folder as the original(s).

### 2 ImageJ Plugin ###
2.1 Installation

Download and unpack IhcLungCancerSeg.zip from the download page. Copy the contents into your ../ImageJ/plugins folder or drag&drop them onto the ImageJ application.
You should now have a submenu 'IHC Lung Cancer Segmentation' in the 'Plugins' menu. (restarting ImageJ might be necessary)

2.2 Usage

The plugin can be used to segment single images or a folder of images, without having to open them explicitly.  
To process a single image, go to 'Plugins>IHC Lung Cancer Segmentation>Choose source file...'.  
To process a folder of images, go to 'Plugins>IHC Lung Cancer Segmentation>Choose source folder...'. The application will process any *.jpg files within the chosen folder.  
In either case you will be asked to use your own visual dictionary instead of the provided data. See notes below.  
The resulting image(s) will be saved in the same folder as the originals.

You can also process an image opened in ImageJ by choosing 'Plugins>IHC Lung Cancer Segmentation>Process current image...'.  
You will be prompted to choose a destination folder, where a copy of the original and the segmented image will be saved.  
Again you may use your own visual dictionary or the pre-calculated data. See notes below.

### 3 Notes on using your own visual dictionary ###

Due to how the application was developed and tested, please note the following if you choose to use your own visual dictionary:  
- The folder you pass to the application should have two subfolders named 'vital' and 'necr'.  
- Each subfolder should contain at least 800 images of size 300x300 pixels of tumor tissue at 40x magnification.  
- The computation of the dictionary will take an additional 5-10 minutes.  


## Contact ##