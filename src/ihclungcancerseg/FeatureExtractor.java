/*
 * Copyright 2016, Florian Wassmer, Valeria De Luca, Ruben Casanova
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ihclungcancerseg;

import ij.IJ;
import ij.blob.*;
import ij.ImagePlus;
import ij.ImageStack;
import ij.WindowManager;
import ij.io.FileSaver;
import ij.plugin.filter.RankFilters;
import static ij.plugin.filter.RankFilters.*;
import ij.process.ColorSpaceConverter;
import ij.process.FloatProcessor;
import ij.process.ImageConverter;
import ij.process.ImageProcessor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 *
 * @author Florian Wassmer
 */
public class FeatureExtractor {
    
    // ------- static variables: -------
    public final static int NB_FEATS = 43;
    
    public final static double PIX_LEN_UM = 0.23;   // 1 pixel(40x) = 0.23 um
    private final static double MINAREA_NUC = 5.;   // size thresholds for vital
    private final static double MAXAREA_NUC = 130.; // nucleus in um^2
    
    private final static double OL_FILT_RAD = 3.;   // radius and threshold for
    private final static float OL_FILT_TH = 20;     // remove-outliers filter
    
    // ------- fields: -------
    private String filePath;
    private ImagePlus origImg;
    
    private int nbBlobs;             // number of blue blobs
    private int totArea;             // total area of blue blobs in pixels
    private double meanArea;         // mean area of blobs in pixels
    private double varArea;          // variance " in pixels
    private double meanLen;          // mean of length(min bound rect) in pixels
    private double varLen;           // var " in pixels
    private int nbBlobsInAreaThresh; // nb of blobs w/in the area thresholds
    private double meanPerimeter;    // further features given by lib, all in pix
    private double meanFeretDiam;
    private double meanMinFeretDiam;
    private double meanShortLen;
    private double meanAspectRatio;
    private double meanCircularity;
    private double meanElongation;
    private double meanConvexity;
    private double meanSolidity;
    private double meanThinnesRatio;
    private double meanTempOuterCont;
    private int histoMedian;
    private int histoMode;
    private double histoSkew;
    private int smallBlobs;
    private int bigBlobs;
    // same for log-image
    private int nbBlobsLog;          
    private int totAreaLog;            
    private double meanAreaLog;    
    private double varAreaLog;      
    private double meanLenLog;    
    private double varLenLog;      
    private int nbBlobsInAreaThreshLog; 
    private double meanPerimeterLog;    
    private double meanFeretDiamLog;
    private double meanMinFeretDiamLog;
    private double meanShortLenLog;
    private double meanAspectRatioLog;
    private double meanCircularityLog;
    private double meanElongationLog;
    private double meanConvexityLog;
    private double meanSolidityLog;
    private double meanThinnesRatioLog;
    private double meanTempOuterContLog;
    private int smallBlobsLog;
    private int bigBlobsLog;
    
    
    // ------- constructors: -------
    public FeatureExtractor(){
        filePath = "";
        origImg = null;
        totArea = totAreaLog = nbBlobs = nbBlobsLog = 0;
        meanArea = meanAreaLog = varArea = varAreaLog =
                meanLen = meanLenLog = varLen = varLenLog = 0.;
        nbBlobsInAreaThresh = nbBlobsInAreaThreshLog = 0;
        meanPerimeter = meanFeretDiam = meanMinFeretDiam = meanShortLen =
                meanAspectRatio = meanCircularity = meanElongation = 
                meanConvexity = meanSolidity = meanThinnesRatio = 
                meanTempOuterCont =
                meanPerimeterLog = meanFeretDiamLog = meanMinFeretDiamLog =
                meanShortLenLog =
                meanAspectRatioLog = meanCircularityLog = meanElongationLog = 
                meanConvexityLog = meanSolidityLog = meanThinnesRatioLog = 
                meanTempOuterContLog = 0.;
    }
    
    public FeatureExtractor(String aFilePath){
        filePath = aFilePath;
        origImg = new ImagePlus(filePath);
        totArea = totAreaLog = nbBlobs = nbBlobsLog = 0;
        meanArea = meanAreaLog = varArea = varAreaLog =
                meanLen = meanLenLog = varLen = varLenLog = 0.;
        nbBlobsInAreaThresh = nbBlobsInAreaThreshLog = 0;
        meanPerimeter = meanFeretDiam = meanMinFeretDiam = meanShortLen =
                meanAspectRatio = meanCircularity = meanElongation = 
                meanConvexity = meanSolidity = meanThinnesRatio = 
                meanTempOuterCont =
                meanPerimeterLog = meanFeretDiamLog = meanMinFeretDiamLog =
                meanShortLenLog =
                meanAspectRatioLog = meanCircularityLog = meanElongationLog = 
                meanConvexityLog = meanSolidityLog = meanThinnesRatioLog = 
                meanTempOuterContLog = 0.;
        histoMedian = histoMode = 0;
        histoSkew = 0.;
    }
    
    public FeatureExtractor(ImagePlus img){
        filePath = "";
        origImg = img;
        totArea = totAreaLog = nbBlobs = nbBlobsLog = 0;
        meanArea = meanAreaLog = varArea = varAreaLog =
                meanLen = meanLenLog = varLen = varLenLog = 0.;
        nbBlobsInAreaThresh = nbBlobsInAreaThreshLog = 0;
        meanPerimeter = meanFeretDiam = meanMinFeretDiam = meanShortLen =
                meanAspectRatio = meanCircularity = meanElongation = 
                meanConvexity = meanSolidity = meanThinnesRatio = 
                meanTempOuterCont =
                meanPerimeterLog = meanFeretDiamLog = meanMinFeretDiamLog =
                meanShortLenLog =
                meanAspectRatioLog = meanCircularityLog = meanElongationLog = 
                meanConvexityLog = meanSolidityLog = meanThinnesRatioLog = 
                meanTempOuterContLog = 0.;
        histoMedian = histoMode = 0;
        histoSkew = 0.;
    }
    
    // names of features:
    public static String[] getFeatNames(){
        String[] names = {"nbBlobs", "totArea", "meanArea", "stdArea", "meanLen",
                          "stdLen", "nbBlobsInAreaThresh", "meanPerimeter",
                          "meanFeretDiam", "meanMinFeretDiam", "meanShortLen",
                          "meanAspectRatio", "meanCircularity", "meanElongation",
                          "meanConvexity", "meanSolidity", "meanThinnesRatio",
                          "meanTempOuterCont", "histoMedian", "histoMode", "histoSkew",
                          "smallBlobs", "bigBlobs",
                          "nbBlobsLog", "totAreaLog", "meanAreaLog", "stdAreaLog", "meanLenLog",
                          "stdLenLog", "nbBlobsInAreaThreshLog", "meanPerimeterLog",
                          "meanFeretDiamLog", "meanMinFeretDiamLog", "meanShortLenLog",
                          "meanAspectRatioLog", "meanCircularityLog", "meanElongationLog",
                          "meanConvexityLog", "meanSolidityLog", "meanThinnesRatioLog",
                          "meanTempOuterContLog",
                          "smallBlobsLog", "bigBlobsLog"};
        return names;
    }
    
    // fields access:
    public double[] getResults(){
        double[] results = new double[NB_FEATS];
        results[0] = nbBlobs;
        results[1] = totArea;
        results[2] = meanArea;
        results[3] = Math.sqrt(varArea);
        results[4] = meanLen;
        results[5] = Math.sqrt(varLen);
        results[6] = nbBlobsInAreaThresh;
        results[7] = meanPerimeter;
        results[8] = meanFeretDiam;
        results[9] = meanMinFeretDiam;
        results[10] = meanShortLen;
        results[11] = meanAspectRatio;
        results[12] = meanCircularity;
        results[13] = meanElongation;
        results[14] = meanConvexity;
        results[15] = meanSolidity;
        results[16] = meanThinnesRatio;
        results[17] = meanTempOuterCont;
        results[18] = histoMedian;
        results[19] = histoMode;
        results[20] = histoSkew;
        results[21] = smallBlobs;
        results[22] = bigBlobs;
        results[23] = nbBlobsLog;
        results[24] = totAreaLog;
        results[25] = meanAreaLog;
        results[26] = Math.sqrt(varAreaLog);
        results[27] = meanLenLog;
        results[28] = Math.sqrt(varLenLog);
        results[29] = nbBlobsInAreaThreshLog;
        results[30] = meanPerimeterLog;
        results[31] = meanFeretDiamLog;
        results[32] = meanMinFeretDiamLog;
        results[33] = meanShortLenLog;
        results[34] = meanAspectRatioLog;
        results[35] = meanCircularityLog;
        results[36] = meanElongationLog;
        results[37] = meanConvexityLog;
        results[38] = meanSolidityLog;
        results[39] = meanThinnesRatioLog;
        results[40] = meanTempOuterContLog;
        results[41] = smallBlobsLog;
        results[42] = bigBlobsLog;
        return results;
    }
    
    // ------- processing: -------
    public void analyze(){
        ImagePlus patchImg = origImg.duplicate();
        ImageProcessor patchIp = patchImg.getProcessor();
        int[] rgbPixelVec = (int[]) patchIp.getPixels();
        
        // histogram features:
        int[] histogram = patchIp.getHistogram();
        int hLen = histogram.length; //256
        int[] sortedHisto = new int[hLen];
        System.arraycopy(histogram, 0, sortedHisto, 0, hLen);
        Arrays.sort(sortedHisto);
        int middle = hLen / 2; //128
        histoMedian = (sortedHisto[middle] + sortedHisto[middle - 1]) / 2;
        double mean = 0.;
        for (int i = 0; i < hLen; i++){
            if (histogram[i] > histogram[histoMode]){
                histoMode = i;
            }
            mean += histogram[i];
        }
        mean = mean / hLen;
        double mu2 = 0.;
        double mu3 = 0.;
        for (int i = 0; i < hLen; i++){
            double dev = histogram[i] - mean;
            mu2 += dev * dev;
            mu3 += dev * dev * dev;
        }
        mu2 = mu2 / hLen;
        mu3 = mu3 / hLen;
        histoSkew = mu3 / (Math.pow(mu2, 1.5));
        
        // nuclei features:
        
        // -------------- based on color thresholding in b-channel ------------
        // convert to Lab, get b-channel:
        ColorSpaceConverter csc = new ColorSpaceConverter();
        ImagePlus labImg = csc.RGBToLab(origImg);
        ImageStack labStack = labImg.getStack();
        ImageProcessor bIp = labStack.getProcessor(3);
        float[] bPixelVec = (float[]) bIp.getPixels();
        
        // mark blue pixels 
        for (int i = 0; i < rgbPixelVec.length; i++) {
            if (bPixelVec[i] < 0){
                rgbPixelVec[i] = 0; // set to black
            } else {
                rgbPixelVec[i] = 0xffffff; // set to white
            }
        }
        patchIp.setPixels(rgbPixelVec);
        
        // filter out areas of a few pixels:
        RankFilters rf = new RankFilters();
        rf.rank(patchIp, OL_FILT_RAD, OUTLIERS, DARK_OUTLIERS, OL_FILT_TH);
        
        // get blobs (need 8-bit binary picture):
        ImagePlus threshImg = new ImagePlus("title", patchIp);
        ImageConverter ic = new ImageConverter(threshImg);
        ic.convertToGray8();
        // --------------------------------------------------------------------
        
        // ------- based on laplacian of gaussian -----------------------------
         ic = new ImageConverter(origImg);
        ic.convertToGray8();
        ImageProcessor grayIp = origImg.getProcessor();
        ImageProcessor floatIp = grayIp.convertToFloat();
        Mexican_Hat_Filter log = new Mexican_Hat_Filter();
        log.run(floatIp);
        grayIp = floatIp.convertToByte(false);
        // create binary image
        byte[] pixels = (byte[])grayIp.getPixels();
        for (int i = 0; i < pixels.length; i++){
            if (pixels[i] < 0){
                pixels[i] = 0;
            } else {
                pixels[i] = (byte) 255;
            }
        }
        ImagePlus logImg = new ImagePlus("title", grayIp);
        
        // --------------------------------------------------------------------
        
        ManyBlobs allBlobs = new ManyBlobs(threshImg);
        allBlobs.findConnectedComponents();
        // extract parameters
        for (Blob blob : allBlobs){
            double area = blob.getEnclosedArea();
            if (area > 10){
                // ignore blobs that are still smaller than 10 pixels
                nbBlobs++;
                totArea += area;
                double areaUm = area * PIX_LEN_UM * PIX_LEN_UM;
                if (areaUm > MINAREA_NUC && areaUm < MAXAREA_NUC){
                    nbBlobsInAreaThresh++;
                }
                if (areaUm < MINAREA_NUC + (MAXAREA_NUC - MINAREA_NUC) * .20){
                    smallBlobs++;
                }
                if (areaUm > MAXAREA_NUC - (MAXAREA_NUC - MINAREA_NUC) * .20){
                    bigBlobs++;
                }
                meanLen += blob.getLongSideMBR();
                meanPerimeter += blob.getPerimeter();
                meanFeretDiam += blob.getFeretDiameter();
                meanMinFeretDiam += blob.getMinFeretDiameter();
                meanShortLen += blob.getShortSideMBR();
                meanAspectRatio += blob.getAspectRatio();
                meanCircularity += blob.getCircularity();
                meanElongation += blob.getElongation();
                meanConvexity += blob.getConvexity();
                meanSolidity += blob.getSolidity();
                meanThinnesRatio += blob.getThinnesRatio();
                meanTempOuterCont += blob.getContourTemperature();
            }
            
        }
        if (nbBlobs > 0){
            meanArea = totArea / nbBlobs;
            meanLen = meanLen / nbBlobs;
            meanPerimeter = meanPerimeter / nbBlobs;
            meanFeretDiam = meanFeretDiam / nbBlobs;
            meanMinFeretDiam = meanMinFeretDiam / nbBlobs;
            meanShortLen = meanShortLen / nbBlobs;
            meanAspectRatio = meanAspectRatio / nbBlobs;
            meanCircularity = meanCircularity / nbBlobs;
            meanElongation = meanElongation / nbBlobs;
            meanConvexity = meanConvexity / nbBlobs;
            meanSolidity = meanSolidity / nbBlobs;
            meanThinnesRatio = meanThinnesRatio / nbBlobs;
            meanTempOuterCont = meanTempOuterCont / nbBlobs;
            
            // variances:
            for (Blob blob: allBlobs){
                double area = blob.getEnclosedArea();
                if (area > 10){
                    double len = blob.getLongSideMBR();
                    varArea += (area - meanArea) * (area - meanArea);
                    varLen += (len - meanLen) * (len - meanLen);
                }

            }
            varArea = varArea / nbBlobs;
            varLen = varLen / nbBlobs;
        }
        
        // ---------- same with the log-image -------------
        
        allBlobs = new ManyBlobs(logImg);
        allBlobs.findConnectedComponents();
        // extract parameters
        for (Blob blob : allBlobs){
            double area = blob.getEnclosedArea();
            if (area > 10){
                // ignore blobs that are still smaller than 10 pixels
                nbBlobsLog++;
                totAreaLog += area;
                double areaUm = area * PIX_LEN_UM * PIX_LEN_UM;
                if (areaUm > MINAREA_NUC && areaUm < MAXAREA_NUC){
                    nbBlobsInAreaThreshLog++;
                }
                if (areaUm < MINAREA_NUC + (MAXAREA_NUC - MINAREA_NUC) * .20){
                    smallBlobsLog++;
                }
                if (areaUm > MAXAREA_NUC - (MAXAREA_NUC - MINAREA_NUC) * .20){
                    bigBlobsLog++;
                }
                meanLenLog += blob.getLongSideMBR();
                meanPerimeterLog += blob.getPerimeter();
                meanFeretDiamLog += blob.getFeretDiameter();
                meanMinFeretDiamLog += blob.getMinFeretDiameter();
                meanShortLenLog += blob.getShortSideMBR();
                meanAspectRatioLog += blob.getAspectRatio();
                meanCircularityLog += blob.getCircularity();
                meanElongationLog += blob.getElongation();
                meanConvexityLog += blob.getConvexity();
                meanSolidityLog += blob.getSolidity();
                meanThinnesRatioLog += blob.getThinnesRatio();
                meanTempOuterContLog += blob.getContourTemperature();
            }
            
        }
        if (nbBlobs > 0){
            meanAreaLog = totAreaLog / nbBlobsLog;
            meanLenLog = meanLenLog / nbBlobsLog;
            meanPerimeterLog = meanPerimeterLog / nbBlobsLog;
            meanFeretDiamLog = meanFeretDiamLog / nbBlobsLog;
            meanMinFeretDiamLog = meanMinFeretDiamLog / nbBlobsLog;
            meanShortLenLog = meanShortLenLog / nbBlobsLog;
            meanAspectRatioLog = meanAspectRatioLog / nbBlobsLog;
            meanCircularityLog = meanCircularityLog / nbBlobsLog;
            meanElongationLog = meanElongationLog / nbBlobsLog;
            meanConvexityLog = meanConvexityLog / nbBlobsLog;
            meanSolidityLog = meanSolidityLog / nbBlobsLog;
            meanThinnesRatioLog = meanThinnesRatioLog / nbBlobsLog;
            meanTempOuterContLog = meanTempOuterContLog / nbBlobsLog;
            
            // variances:
            for (Blob blob: allBlobs){
                double area = blob.getEnclosedArea();
                if (area > 10){
                    double len = blob.getLongSideMBR();
                    varAreaLog += (area - meanAreaLog) * (area - meanAreaLog);
                    varLenLog += (len - meanLenLog) * (len - meanLenLog);
                }

            }
            varAreaLog = varAreaLog / nbBlobsLog;
            varLenLog = varLenLog / nbBlobsLog;
        }
        
    }
    
}
