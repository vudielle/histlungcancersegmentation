/*
 * Copyright 2016, Florian Wassmer, Valeria De Luca, Ruben Casanova
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ihclungcancerseg;

import ij.ImagePlus;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author Florian Wassmer
 */
public class Utils {
    
    public static double getDistance(double[] vec1, double[] vec2){
        int nbFeats = FeatureExtractor.NB_FEATS;
        double squareDist = 0.;
        if (vec1.length == vec2.length){
            for (int i = 0; i < nbFeats; i++){
                squareDist += (vec1[i] - vec2[i]) * (vec1[i] - vec2[i]);
            }
        } else {
            System.out.println("unequal dims in getDistance!");
        }
        return Math.sqrt(squareDist);
    }
    
    // auxiliary class to filter for jpgs
    public static class JpgFilter implements FileFilter {
        @Override
        public boolean accept(File file){
            boolean accept = false;
            String path = file.getAbsolutePath();
            int ind = path.lastIndexOf(".");
            if (ind > 0 && path.substring(ind).equals(".jpg")){
                accept = true;
            }
            return accept;
        }
    }
    
    // auxiliary class to filter for tifs
    public static class TifFilter implements FileFilter {
        @Override
        public boolean accept(File file){
            boolean accept = false;
            String path = file.getAbsolutePath();
            int ind = path.lastIndexOf(".");
            if (ind > 0 && path.substring(ind).equals(".tif")){
                accept = true;
            }
            return accept;
        }
    }
}
