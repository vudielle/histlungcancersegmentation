/*
 * Copyright 2016, Florian Wassmer, Valeria De Luca, Ruben Casanova
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ihclungcancerseg;

import ij.IJ;
import ij.ImagePlus;
import ij.io.FileSaver;
import ij.plugin.filter.ParticleAnalyzer;
import ij.plugin.filter.RankFilters;
import ij.plugin.frame.ColorThresholder;
import ij.process.AutoThresholder;
import ij.process.ByteProcessor;
import ij.process.ColorProcessor;
import ij.process.ImageConverter;
import ij.process.ImageProcessor;
import java.io.File;
import java.io.FileFilter;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;
import sc.fiji.CMP_BIA.segmentation.structures.Labelling2D;
import sc.fiji.CMP_BIA.segmentation.superpixels.jSLIC;

/**
 *
 * @author Florian Wassmer
 */
public class ImageSegmenter {
            
//  ------------ static variables -------------
    private final static int PATCH_SIZE = 300;
    private final static int PATCH_NB = 800;
    private final static int STEP = 50;
    private final static int K_NBRS = 15;
    private final static int SP_SIZE = 100;
    private final static float SP_REGL = (float)0.1;
    
    // dummy stream to prevent output from third party library
    private final static PrintStream DUMMY_STREAM = 
        new PrintStream(new OutputStream(){
            @Override
            public void write(int b) {
                //NO-OP
            }       
    });
    
    private final String sourceDir;
    private final File[] imgFiles;
    private final String dictionaryDir;
    private final boolean useOwnDict;
    private double[][] featSpaceV;
    private double[][] featSpaceN;
    private double[] maxFeats;
    
    private long timer;
    
    
    // constructor - if called from command line or from imagej open image
    public ImageSegmenter(String sourcePath, String dictionaryPath, boolean useOwnDict){
        File sourceFile = new File(sourcePath);
        if (sourceFile.isDirectory()){
            sourceDir = sourcePath;
            FileFilter filter = new Utils.JpgFilter();
            imgFiles = sourceFile.listFiles(filter);
        } else if (sourceFile.isFile()){
            sourceDir = sourceFile.getParent();
            imgFiles = new File[1];
            imgFiles[0] = sourceFile;
        } else {
            sourceDir = "";
            imgFiles = null;
        }
        dictionaryDir = dictionaryPath;
        this.useOwnDict = useOwnDict;
    }
    
    // constructor - if called from imagej on current image
    public ImageSegmenter(ImageProcessor ip, String savePath, String origName, String dictPath, boolean useOwnDict){
        sourceDir = savePath;
        dictionaryDir = dictPath;
        ImagePlus origImg = new ImagePlus("", ip);
        FileSaver fs = new FileSaver(origImg);
        String imgPath = savePath + File.separator + origName + "_ihc.jpg";
        fs.saveAsJpeg(imgPath);
        imgFiles = new File[1];
        imgFiles[0] = new File(imgPath);
        this.useOwnDict = useOwnDict;
    }
    
    // segments the original images using the given dictionary of vital tumor and necrosis
    // saves the result into the same folder as the original
    public void doSegmentation(){
        boolean success = false;
        if (useOwnDict){
            success = buildFeatureSpace(null);
        } else {
            success = loadFeatureSpace();
        }
        if (!success){
            System.out.println("Failed building dictionary.");
            return;
        }
        
        for (File imgFile : imgFiles){
            ImagePlus origImg = new ImagePlus(imgFile.getAbsolutePath());
            System.out.println("segmenting " + origImg.getTitle() + "...");
            timer = System.currentTimeMillis();
            success = doSegmentation(origImg);
            timer = System.currentTimeMillis() - timer;
            if (success) {
                System.out.println("done. time elapsed: " + timer / 1000. + "s");
            } else {
                System.out.println("failed. time elapsed: " + timer / 1000. + "s");
            }
        }
    }
    
    private boolean doSegmentation(ImagePlus origImg){
        boolean success;
        String savePath = origImg.getShortTitle();
        savePath = sourceDir + File.separator + savePath + "_seg.tif";
        FileSaver fs;
            
        // rule out stroma (get img w/ tumor tissue: black, rest: white)
        ImagePlus binaryImg = createBinaryImg(origImg);
        // segment the image
        ImagePlus outputImg = null;
        outputImg = processImageSp(origImg, binaryImg);
        success = !(outputImg == null);
        
        fs = new FileSaver(outputImg);
        fs.saveAsTiff(savePath);
        
        return success;
    }
    
    // create binary image: tumor vs stroma&background
    private ImagePlus createBinaryImg(ImagePlus origImg){
        ImagePlus copy = origImg.duplicate();

        ij.WindowManager.setTempCurrentImage(copy);
        ColorThresholder.RGBtoLab();
        ImageProcessor labIp = copy.getProcessor();
        int[] labPixels = (int[]) labIp.getPixels();
        //create binary img: brownish color (b>thresh) -> black - tumor
        //                   blueish color (b<thresh) -> white - stroma, background
        ImageProcessor byteIp = new ByteProcessor(origImg.getWidth(), origImg.getHeight());
        ImagePlus byteImg = new ImagePlus("", byteIp);
        byte[] bytePixels = (byte[]) byteIp.getPixels();
        for (int i = 0; i < bytePixels.length; i++) {
            int b = labPixels[i] & 0x0000ff;
            if (b > 127){
                // >127 of 0-255 -> more brownish than blueish
                bytePixels[i] = 0; // set to black
            } else {
                bytePixels[i] = (byte) 255; // set to white
            }
        }
        
        // filter with area threshold (imagej - analyze particles):
        int options = ParticleAnalyzer.CLEAR_WORKSHEET | 
                ParticleAnalyzer.IN_SITU_SHOW | ParticleAnalyzer.SHOW_MASKS;
        int measurements = 0;
        double minSize = 5000.;     // upper threshold for nuclei: ~250mum^2
        double maxSize = 10000000.;
        ParticleAnalyzer pa = new ParticleAnalyzer(options, measurements, null, minSize, maxSize);
        // first: remove too small black particles outside of tumor
        pa.analyze(byteImg);
        byteIp = byteImg.getProcessor();
        bytePixels = (byte[]) byteIp.getPixels();
        // invert and analyze again to fill small white particles (nuclei)
        // (invert because ParticleAnalzyer works on black particles)
        for (int i = 0; i < bytePixels.length; i++){
            if (bytePixels[i] == 0){
                bytePixels[i] = (byte) 255;
            } else {
                bytePixels[i] = 0;
            }
        }
        pa = new ParticleAnalyzer(options, measurements, null, minSize, maxSize);
        pa.analyze(byteImg);
        byteIp = byteImg.getProcessor();
        bytePixels = (byte[]) byteIp.getPixels();
        // invert back to black=tumor, white=rest
        for (int i = 0; i < bytePixels.length; i++){
            if (bytePixels[i] == 0){
                bytePixels[i] = (byte) 255;
            } else {
                bytePixels[i] = 0;
            }
        }
        byteImg.setProcessor(byteIp);
        return byteImg;
    }
    
    // create background image: only background white, rest black
    private ImagePlus createBackgroundImg(ImagePlus origImg){
        int[] origPixels = (int[]) origImg.getProcessor().getPixels();
        //create binary img: very bright (r,g,b > 200) -> background: white
        ImageProcessor byteIp = new ByteProcessor(origImg.getWidth(), origImg.getHeight());
        ImagePlus byteImg = new ImagePlus("", byteIp);
        byte[] bytePixels = (byte[]) byteIp.getPixels();
        for (int i = 0; i < bytePixels.length; i++) {
            int r = (origPixels[i] & 0xff0000) >> 16;
            int g = (origPixels[i] & 0x00ff00) >> 8;
            int b = origPixels[i] & 0x0000ff;
            if (r > 100 && g > 100 && b > 190){
                bytePixels[i] = (byte) 255; // set to white
            } else {
                bytePixels[i] = 0; // set to black
            }
        }
        return byteImg;
    }
    
    // load feature space from file
    private boolean loadFeatureSpace(){
        boolean success = true;
        System.out.println("Loading dictionary...");
        try {
            InputStream fis = this.getClass().getResourceAsStream("dictionary.dat");
            ObjectInputStream ois = new ObjectInputStream(fis);
            featSpaceV = (double[][]) ois.readObject();
            featSpaceN = (double[][]) ois.readObject();
            maxFeats = (double[]) ois.readObject();
            ois.close();
        }
        catch(Exception e) {
             System.out.println(e.getMessage());
        }
        System.out.println("done.");
        return success;
    }
    
    // build feature space form dictionaryDir, excluding patches from excludeImg
    private boolean buildFeatureSpace(ImagePlus excludeImg){
        timer = System.currentTimeMillis();
        boolean success;
        System.out.println("building feature space... (this may take some minutes)");
        FeatureExtractor fe;
        String dirV = dictionaryDir + File.separator + "vital";
        String dirN = dictionaryDir + File.separator + "necr";
        File dictFileV = new File(dirV);
        File dictFileN = new File(dirN);
        File[] patsV = dictFileV.listFiles();
        File[] patsN = dictFileN.listFiles();
        ArrayList<File> dictionaryV;
        ArrayList<File> dictionaryN;
        if (excludeImg == null){
            // build dict from all patches
            dictionaryV = new ArrayList<File>(Arrays.asList(patsV));
            dictionaryN = new ArrayList<File>(Arrays.asList(patsN));
        } else {
            // build dict without patches from excludeImg
            dictionaryV = new ArrayList<File>();
            dictionaryN = new ArrayList<File>();
            for (File patV : patsV){
                if (!(patV.getName().contains(excludeImg.getShortTitle()))){
                    dictionaryV.add(patV);
                }
            }
            for (File patN : patsN){
                if (!(patN.getName().contains(excludeImg.getShortTitle()))){
                    dictionaryN.add(patN);
                }
            }
        }
        featSpaceV = new double[PATCH_NB][FeatureExtractor.NB_FEATS];
        featSpaceN = new double[PATCH_NB][FeatureExtractor.NB_FEATS];
        maxFeats = new double[FeatureExtractor.NB_FEATS];
        Random rand = new Random();
        success = true;
        for (int i = 0; i < PATCH_NB; i++) {
            if (dictionaryV.size() > 0){
                int ind = rand.nextInt(dictionaryV.size());
                File patch = dictionaryV.get(ind);
                ImagePlus patchImg = new ImagePlus(patch.getAbsolutePath());
                ImageProcessor patchIp = patchImg.getProcessor();
                int[] patchPix = (int[]) patchIp.getPixels();
                // recolor background influence
                for (int p = 0; p < patchPix.length; p++){
                    int col = patchPix[p];
                    int r = (col & 0xff0000) >> 16;
                    int g = (col & 0x00ff00) >> 8;
                    int b = col & 0x0000ff;
                    if (r > 100 && g > 100 && b > 190){
                        // recolor
                        patchPix[p] = 0x503000; //brown
                    }
                }
                patchIp.setPixels(patchPix);
                patchImg.setProcessor(patchIp);
                fe = new FeatureExtractor(patchImg);
                fe.analyze();
                featSpaceV[i] = fe.getResults();
                for (int j = 0; j < FeatureExtractor.NB_FEATS; j++){
                    if (Math.abs(featSpaceV[i][j]) > maxFeats[j]){
                        maxFeats[j] = Math.abs(featSpaceV[i][j]);
                    }
                }
                dictionaryV.remove(ind);
            } else {
                success = false;
                break;
            }
            if (dictionaryN.size() > 0){
                int ind = rand.nextInt(dictionaryN.size());
                File patch = dictionaryN.get(ind);
                ImagePlus patchImg = new ImagePlus(patch.getAbsolutePath());
                ImageProcessor patchIp = patchImg.getProcessor();
                int[] patchPix = (int[]) patchIp.getPixels();
                // recolor background influence
                for (int p = 0; p < patchPix.length; p++){
                    int col = patchPix[p];
                    int r = (col & 0xff0000) >> 16;
                    int g = (col & 0x00ff00) >> 8;
                    int b = col & 0x0000ff;
                    if (r > 100 && g > 100 && b > 190){
                        // recolor
                        patchPix[p] = 0x503000; //brown
                    }
                }
                patchIp.setPixels(patchPix);
                patchImg.setProcessor(patchIp);
                fe = new FeatureExtractor(patchImg);
                fe.analyze();
                featSpaceN[i] = fe.getResults();
                for (int j = 0; j < FeatureExtractor.NB_FEATS; j++){
                    if (Math.abs(featSpaceN[i][j]) > maxFeats[j]){
                        maxFeats[j] = Math.abs(featSpaceN[i][j]);
                    }
                }
                dictionaryN.remove(ind);
            } else {
                success = false;
                break;
            }
        }
        // normalize all feats to 0..100:
        for (int i = 0; i < featSpaceV.length; i++){
            for (int j = 0; j < FeatureExtractor.NB_FEATS; j++){
                int k = j % 23;
                if (k < 3 || k == 4 || (k > 5 && k < 11) || j == 21 || j == 22 || j > 40){
                    // heavier features
                    featSpaceV[i][j] = featSpaceV[i][j] / maxFeats[j] * 200;
                    featSpaceN[i][j] = featSpaceN[i][j] / maxFeats[j] * 200;
                } else if (k == 5 || (k > 11 && k < 16) || k == 17 || j == 19) {
                    // normal features
                    featSpaceV[i][j] = featSpaceV[i][j] / maxFeats[j] * 100.;
                    featSpaceN[i][j] = featSpaceN[i][j] / maxFeats[j] * 100.;
                }
                else {
                    // weak features
                    featSpaceV[i][j] = featSpaceV[i][j] / maxFeats[j] * 50;
                    featSpaceN[i][j] = featSpaceN[i][j] / maxFeats[j] * 50;
                }
            }
        }
        timer = System.currentTimeMillis() - timer;
        System.out.println("done. time elapsed: " + timer / 1000. + "s");
        
        return success;
    }
    
    private boolean buildFeatureSpace(){
        boolean success = buildFeatureSpace(null);
        return success;
    }
    
    private ImagePlus processImageSp(ImagePlus origImg, ImagePlus binaryImg){
        ImageProcessor origIp = origImg.getProcessor();
        int[] origPixels = (int[]) origIp.getPixels();
        ImageProcessor binaryIp = binaryImg.getProcessor();
        byte[] binaryPixels = (byte[]) binaryIp.getPixels();
        ImagePlus outputImg = binaryImg.duplicate();
        ImageConverter ic = new ImageConverter(outputImg);
        ic.convertToRGB();
        ImageProcessor outputIp = outputImg.getProcessor();
        int[] outputPixels = (int[]) outputIp.getPixels();
        double[] outputHeatMap = new double[outputPixels.length];
        double maxHeat = 0.;
        int nx = origImg.getWidth();
        int ny = origImg.getHeight();
        
        // superpixel processing:
        PrintStream out = System.out;
        System.setOut(DUMMY_STREAM);
        jSLIC superpixels = new jSLIC(origImg);
        superpixels.process(SP_SIZE, SP_REGL);
        System.setOut(out);
        Labelling2D spLabels = superpixels.getSegmentation();
        int nbSuperpixels = superpixels.getNbLabels();
        ArrayList<Integer>[] spIndices = (ArrayList<Integer>[])new ArrayList[nbSuperpixels];
        int[][] spCenters = new int[nbSuperpixels][2];
        for (int label = 0; label < nbSuperpixels; label++){
            spIndices[label] = new ArrayList<Integer>();
        }
        for (int pixel = 0; pixel < nx * ny; pixel++){
            if (binaryPixels[pixel] != 0){
                // only interested in tumor pixels - ==0 would be bckgr/stroma
                int label = spLabels.getLabel(pixel % nx, pixel / nx);
                spIndices[label].add(pixel);
                spCenters[label][0] += pixel % nx;
                spCenters[label][1] += pixel / nx;
            }
        }
        
        ImagePlus bckImg = createBackgroundImg(origImg);
        ImageProcessor bckIp = bckImg.getProcessor();
        
        // actual processing, superpixelwise:
        for (int label = 0; label < nbSuperpixels; label++){
            int n = spIndices[label].size();
            if (n < 0.1 * (SP_SIZE * SP_SIZE)){
                // only few tumor pixels in superpixel - ignore
                continue;
            }
            // else continue with evaluation
            int centerX = spCenters[label][0] / n;
            int centerY = spCenters[label][1] / n;
            
            // coo's of top left corner to later cut patch:
            int tlx = centerX - PATCH_SIZE / 2;
            int tly = centerY - PATCH_SIZE / 2;
            // reassign if patch partially outside of img
            tlx = Math.max(0, Math.min(tlx, nx - PATCH_SIZE));
            tly = Math.max(0, Math.min(tly, ny - PATCH_SIZE));
            
            // check if patch is along the 'tumor-edge'
            // (neccessary for 'isVital'-function)
            bckImg.setRoi(tlx, tly, PATCH_SIZE, PATCH_SIZE);
            ImageProcessor patchBckIp = bckIp.crop();
            byte[] bckPixels = (byte[]) patchBckIp.getPixels();
            int nbBck = 0;
            double centerMassBckX = 0;
            double centerMassBckY = 0;
            for (int i = 0; i < bckPixels.length; i++){
                byte pix = bckPixels[i];
                if (pix != 0){
                    nbBck++;
                    centerMassBckX += i % PATCH_SIZE;
                    centerMassBckY += (i / PATCH_SIZE);
                }
            }
            centerMassBckX = centerMassBckX / nbBck;
            centerMassBckY = centerMassBckY / nbBck;
            int ctOffset = PATCH_SIZE / 7;
            boolean isCenteredX = centerMassBckX > (PATCH_SIZE / 2 - ctOffset) && centerMassBckX < (PATCH_SIZE / 2 + ctOffset);
            boolean isCenteredY = centerMassBckY > (PATCH_SIZE / 2 - ctOffset) && centerMassBckY < (PATCH_SIZE / 2 + ctOffset);
            boolean bckIsCentered = isCenteredX && isCenteredY;
            boolean isEdgePatch = ((nbBck > .1 * (PATCH_SIZE * PATCH_SIZE)) && !bckIsCentered);
            
            // cut patch from original image:
            ImageProcessor patchIp, patchBinIp;
            origImg.setRoi(tlx, tly, (int)(PATCH_SIZE), (int)(PATCH_SIZE));
            patchIp = origIp.crop();
            binaryImg.setRoi(tlx, tly, (int)(PATCH_SIZE), (int)(PATCH_SIZE));
            patchBinIp = binaryIp.crop();
            ImagePlus patchImg = new ImagePlus("", patchIp);
            ImagePlus patchBin = new ImagePlus("", patchBinIp);
            ImagePlus patchBck = new ImagePlus("", patchBckIp);
            
            // do dictionary evaluation:
            double isV = isVital(patchImg, patchBck, patchBin, isEdgePatch);
            
            // assign all pixels within this superpixel according to result:
            for (int pixel : spIndices[label]){
                outputHeatMap[pixel] = isV;
                outputPixels[pixel] = 0x0000ff; // blue = assigned
                if (Math.abs(isV) > maxHeat){
                    maxHeat = Math.abs(isV);
                }
            }
        }
        
        // final output pixel array - with vital in orange, necrotic in blue:
        int[] savepix = new int[outputPixels.length];
        
        // color according to heatmap: scale by max - vital reddish, necr greenish
        for (int i = 0; i < outputPixels.length; i++){
            savepix[i] = 0xffffff;
            if (outputPixels[i] == 0x0000ff){
                // if assigned:
                int red, green, blue;
                double scale = outputHeatMap[i] / maxHeat;
                if (scale > 0){
                    //vital
                    red = 0xff;
                    green = blue = 200 - (int)(200 * scale);
                    savepix[i] = 0xff8800;
                } else {
                    //necrotic
                    green = 0xff;
                    scale = Math.abs(scale);
                    red = blue = 200 - (int)(200 * scale);
                    savepix[i] = 0xff;
                }
                int color = (red << 16) + (green << 8) + (blue);
                outputPixels[i] = color;
            }
        }
                
        // overwrite if connected neighbours are more definitely other class
        double[] oldHeats = Arrays.copyOf(outputHeatMap, outputHeatMap.length);
        for (int label = 0; label < nbSuperpixels; label++){
            int n = spIndices[label].size();
            if (n < 0.1 * (SP_SIZE * SP_SIZE)){
                // only few tumor pixels in superpixel - ignore
                continue;
            }
            // else continue with evaluation
            int centerX = spCenters[label][0] / n;
            int centerY = spCenters[label][1] / n;
            double oldHeat = oldHeats[centerX + nx * centerY];
            ArrayList<Integer> neighbouringSps = 
                    findNeihbouringSps(centerX, centerY, nx, ny, spLabels);
            double neighboursHeat = 0.;
            for (int nLabel : neighbouringSps){
                int nb = spIndices[nLabel].size();
                
                if (nb < 0.1 * (SP_SIZE * SP_SIZE)){
                    // superpixel that wasn't assigned ( = stroma sp)
                } else {
                    int nIndex = spIndices[nLabel].get(0);
                    int color = outputPixels[nIndex] & 0xffffff;
                    neighboursHeat += oldHeats[nIndex];
                }
            }
            // reassign if heat of neighbours bigger than own heat:
            if (Math.abs(neighboursHeat) > Math.abs(oldHeat) && 
                Math.signum(neighboursHeat) != Math.signum(oldHeat) &&
                oldHeat != 0){
                // invert
                double newHeat = 0 - oldHeat;
                // color purple or cyan, accordingly:
                int red, green, blue;
                double scale = newHeat / maxHeat;
                if (scale > 0){
                    //vital
                    red = blue = 0xff;
                    green = 200 - (int)(200 * scale);
                } else {
                    //necrotic
                    green = blue =  0xff;
                    scale = Math.abs(scale);
                    red = 200 - (int)(200* scale);
                }
                int color = (red << 16) + (green << 8) + (blue);
                for (int index : spIndices[label]){
                    outputPixels[index] = color;
                    outputHeatMap[index] = newHeat;
                    if (newHeat > 0){
                        savepix[index] = 0xff8800;
                    } else {
                        savepix[index] = 0xff;
                    }
                }
            }
        }
                
        // now: segmented image, no color in superpixels with too few tumor pixels //  
        // find still unassigned pixels:
        for (int i = 0; i < outputPixels.length; i++) {
            int ix = i % nx;
            int iy = i / nx;
            if ((outputPixels[i] & 0x00ffffff) == 0){  //unassigned if still black
                // assign to adjoining tumor label (if any within square of [SP_SIZE/2])
                // (if only stroma around - leave unassigned (noise/too small to classify)
                boolean assigned = false;
                int itx = Math.max(0, ix - SP_SIZE / 4);
                int ity = Math.max(0, iy - SP_SIZE / 4);
                int maxx = Math.min(itx + SP_SIZE / 2, nx);
                int maxy = Math.min(ity + SP_SIZE / 2, ny);
                while (!assigned && ity < maxy){
                    int pixVal = outputPixels[itx + ity * nx];
                    if (pixVal != 0xffffffff && pixVal != 0xff000000){
                        outputPixels[i] = pixVal;
                        outputHeatMap[i] = outputHeatMap[itx + ity * nx];
                        assigned = true;
                        if ((pixVal & 0xff0000) == 0xff0000){
                            savepix[i] = 0xff8800;
                        } else {
                            savepix[i] = 0xff;
                        }
                    }
                    // update iterators
                    if (itx < maxx - 1){
                        itx++;
                    } else {
                        itx = Math.max(0, ix - SP_SIZE / 4);
                        ity++;
                    }
                }
            }
        }
        
        // now: ignore too little necrosis (most probably vital-only image) //
        // if less than 3% necrotic: ingore, reassign vital
        int nbN = 0;
        int nbV = 0;
        for (int pix : outputPixels){
            if (((pix & 0xff0000) == 0xff0000) && ((pix & 0xff00) != 0xff00)){
                nbV++;
            } else if((pix & 0xff00) == 0xff00 && ((pix & 0xff0000) != 0xff0000)){
                nbN++;
            }
        }
        double npc = (double) nbN / (nbN + nbV);
        if (npc < .03){
            for(int i = 0; i < outputPixels.length; i++){
                if ((outputPixels[i] & 0xff00) == 0xff00 && (outputPixels[i] & 0xff0000) != 0xff0000){
                    // mark orange according to scale
                    double scale = Math.abs(outputHeatMap[i]) / maxHeat;
                    if (scale > .67){
                        // pretty sure it's necrotic - leave
                    } else{
                        int red = 0xff;
                        int green = 228 - (int)(100 * scale);
                        int blue = 200 - (int)(200 * scale);
                        outputPixels[i] = (red << 16) + (green << 8) + blue;
                        savepix[i] = 0xff8800;
                    }
                }
            }
        }
        
        ImageProcessor saveIp = new ColorProcessor(nx, ny, savepix);
        ImagePlus saveImg = new ImagePlus("", saveIp);
        return saveImg;
//        return outputImg;
    }
    
    // find neihgbouring superpixels in 8 directions:
    private ArrayList<Integer> findNeihbouringSps(int x, int y, int nx, int ny,
                                                  Labelling2D labels){
        ArrayList<Integer> neighbours = new ArrayList<Integer>();
        int ownLabel = labels.getLabel(x, y);
        boolean notFound = true;
        int ix = x;
        int iy = y;
        //left:
        while(notFound){
            ix -= 25;
            if (ix < 0){
                notFound = false;
            } else {
                int newLabel = labels.getLabel(ix, iy);
                if (newLabel != ownLabel){
                    notFound = false;
                    if (!neighbours.contains(newLabel)){
                        neighbours.add(newLabel);
                    }
                }
            }
        }
        notFound = true;
        ix = x;
        iy = y;
        //right:
        while(notFound){
            ix += 25;
            if (ix >= nx){
                notFound = false;
            } else {
                int newLabel = labels.getLabel(ix, iy);
                if (newLabel != ownLabel){
                    notFound = false;
                    if (!neighbours.contains(newLabel)){
                        neighbours.add(newLabel);
                    }
                }
            }
        }
        notFound = true;
        ix = x;
        iy = y;
        //up:
        while(notFound){
            iy -= 25;
            if (iy < 0){
                notFound = false;
            } else {
                int newLabel = labels.getLabel(ix, iy);
                if (newLabel != ownLabel){
                    notFound = false;
                    if (!neighbours.contains(newLabel)){
                        neighbours.add(newLabel);
                    }
                }
            }
        }
        notFound = true;
        ix = x;
        iy = y;
        //down:
        while(notFound){
            iy += 25;
            if (iy >= ny){
                notFound = false;
            } else {
                int newLabel = labels.getLabel(ix, iy);
                if (newLabel != ownLabel){
                    notFound = false;
                    if (!neighbours.contains(newLabel)){
                        neighbours.add(newLabel);
                    }
                }
            }
        }
        notFound = true;
        ix = x;
        iy = y;
        //upleft:
        while(notFound){
            ix -= 25;
            iy -= 25;
            if (iy < 0 || ix < 0){
                notFound = false;
            } else {
                int newLabel = labels.getLabel(ix, iy);
                if (newLabel != ownLabel){
                    notFound = false;
                    if (!neighbours.contains(newLabel)){
                        neighbours.add(newLabel);
                    }
                }
            }
        }
        notFound = true;
        ix = x;
        iy = y;
        //upright:
        while(notFound){
            ix += 25;
            iy -= 25;
            if (iy < 0 || ix >= nx){
                notFound = false;
            } else {
                int newLabel = labels.getLabel(ix, iy);
                if (newLabel != ownLabel){
                    notFound = false;
                    if (!neighbours.contains(newLabel)){
                        neighbours.add(newLabel);
                    }
                }
            }
        }
        notFound = true;
        ix = x;
        iy = y;
        //downleft:
        while(notFound){
            ix -= 25;
            iy += 25;
            if (iy >= ny || ix < 0){
                notFound = false;
            } else {
                int newLabel = labels.getLabel(ix, iy);
                if (newLabel != ownLabel){
                    notFound = false;
                    if (!neighbours.contains(newLabel)){
                        neighbours.add(newLabel);
                    }
                }
            }
        }
        notFound = true;
        ix = x;
        iy = y;
        //downright:
        while(notFound){
            ix += 25;
            iy += 25;
            if (iy >= ny || ix >= nx){
                notFound = false;
            } else {
                int newLabel = labels.getLabel(ix, iy);
                if (newLabel != ownLabel){
                    notFound = false;
                    if (!neighbours.contains(newLabel)){
                        neighbours.add(newLabel);
                    }
                }
            }
        }
        return neighbours;
    }
        
    // check with featurespace if patch contains vital tumor or necrosis
    private double isVital(ImagePlus patchImg, ImagePlus patchBckImg, ImagePlus patchBinImg, boolean isEdge){
        ImageProcessor patchIp = patchImg.getProcessor();
        int[] patchPixels = (int[])patchIp.getPixels();
        ImageProcessor bckIp = patchBckImg.getProcessor();
        byte[] bckPixels = (byte[]) bckIp.getPixels();
        ImageProcessor binIp = patchBinImg.getProcessor();
        byte[] binPixels = (byte[]) binIp.getPixels();
        int tumorArea = 0;
        int stromaArea = 0;
        int totArea = PATCH_SIZE * PATCH_SIZE;
        for (int i = 0; i < bckPixels.length; i++){
            if (binPixels[i] == 0 && bckPixels[i] == 0){
                // stroma and not background
                stromaArea++;
            }
        }
        boolean rca = stromaArea < .25 * totArea;
        for (int i = 0; i < bckPixels.length; i++){
            if (binPixels[i] != 0){
                // black - tumor
                tumorArea++;
            } else if (rca){
                // recolor any stroma and background pixels to brown
                patchPixels[i] = 0x503000;
            }
            if (!rca && (bckPixels[i] != 0 && isEdge)){
                patchPixels[i] = 0x503000;
            }
        }
       
        patchIp.setPixels(patchPixels);
        patchImg = new ImagePlus("", patchIp);
        FeatureExtractor fe = new FeatureExtractor(patchImg);
        fe.analyze();
        double[] feats = fe.getResults();
        for (int j = 0; j < FeatureExtractor.NB_FEATS; j++){
            int k = j % 23;
            // scale by tumor area - nbBlobs, totArea, nbBlobsInThresh
            if ((k < 2 || k == 6 || j == 21 || j == 22 || j > 40) && isEdge){
                feats[j] = feats[j] / ((double)tumorArea / totArea);
            }
            // normalize:
            if (k < 3 || k == 4 || (k > 5 && k < 11) || j == 21 || j == 22 || j > 40){
                // heavier features
                feats[j] = feats[j] / maxFeats[j] * 200.;
            } else if (k == 5 || (k > 11 && k < 16) || k == 17 || j == 19){
                // normal features
                feats[j] = feats[j] / maxFeats[j] * 100.;
            }
            else {
                 //weak features
                feats[j] = feats[j] / maxFeats[j] * 50;
            }
        }
        
        // knn-scheme:
        ArrayList<Double> distancesV = new ArrayList<Double>();
        ArrayList<Double> distancesN = new ArrayList<Double>();
        for (double[] featsV : featSpaceV) {
            distancesV.add(Utils.getDistance(feats, featsV));
        }
        for (double[] featsN : featSpaceN) {
            distancesN.add(Utils.getDistance(feats, featsN));
        }
        Collections.sort(distancesV);
        Collections.sort(distancesN);
        int nbVnear = 0;
        int nbNnear = 0;
        double weightV = 0.;
        double weightN = 0.;
        while (nbVnear + nbNnear < K_NBRS){
            double distV = distancesV.get(nbVnear);
            double distN = distancesN.get(nbNnear);
            if (distV < distN){
                nbVnear++;
                if (distV > 0){
                    weightV += 1. / distV;
                } else {
                    weightV += 1.5;
                }
            } else {
                nbNnear++;
                if (distN > 0){
                    weightN += 1. / distN;
                } else {
                    weightN += 1.5;
                }
            }
        }
        return weightV - weightN;
    }
}
