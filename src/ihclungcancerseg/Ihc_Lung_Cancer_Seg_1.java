/*
 * Copyright 2016, Florian Wassmer, Valeria De Luca, Ruben Casanova
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ihclungcancerseg;

import ij.*;
import ij.process.*;
import ij.gui.*;
import ij.io.DirectoryChooser;
import ij.io.OpenDialog;
import java.awt.*;
import ij.plugin.*;
import ij.plugin.frame.*;

/**
 *
 * @author Florian Wassmer
 */
public class Ihc_Lung_Cancer_Seg_1 implements PlugIn{

    private String sourcePath;
    private String dictPath;
    
    @Override
    public void run(String arg) {
        
        if (arg.equals("file")){
            OpenDialog diag;
            diag = new OpenDialog("Choose source file to be segmented: ");
            sourcePath = diag.getPath(); 
        } else if (arg.equals("folder")){
            DirectoryChooser dir;
            dir = new DirectoryChooser("Choose folder with source files to be segmented: ");
            sourcePath = dir.getDirectory();
        }
        
        if (sourcePath == null){
            return;
        }
        
        boolean useOwnDict;
        String msg = "Do you want to use your own visual dictionary?";
        YesNoCancelDialog yncdiag = new YesNoCancelDialog(null, "Use own dictionary?", msg);
        if (yncdiag.cancelPressed()){
            return;
        }
        useOwnDict = yncdiag.yesPressed();
        
        if (useOwnDict){
            DirectoryChooser dir;
            dir = new DirectoryChooser("Choose Visual Dictionary path: ");
            dictPath = dir.getDirectory();
            if (dictPath == null){
                return;
            }
        }
        
        ImageSegmenter imgse = new ImageSegmenter(sourcePath, dictPath, useOwnDict);
        imgse.doSegmentation();
    }
    
}
