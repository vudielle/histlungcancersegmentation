/*
 * Copyright 2016, Florian Wassmer, Valeria De Luca, Ruben Casanova
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ihclungcancerseg;

import java.util.Scanner;

/**
 *
 * @author Florian Wassmer
 */
public class IhcLungCancerSeg {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        String sourcePath;
        String dictPath = "";
        boolean useOwnDict = false;
        Scanner reader = new Scanner(System.in);
        System.out.println("Enter source path (file or folder): ");
        sourcePath = reader.nextLine();
        boolean valid = false;
        System.out.println("Do you want to use your own visual dictionary? (y for yes, n to load default dict)");
        while (!valid){
            String answer = reader.nextLine();
            if (answer.equals("y")){
                valid = true;
                useOwnDict = true;
            } else if (answer.equals("n")){
                valid = true;
                useOwnDict = false;
            } else {
                System.out.println("Please write y or n");
            }
        }
        if (useOwnDict){
            System.out.println("Enter visual dictionary path: ");
            dictPath = reader.nextLine();
        }
        
        ImageSegmenter imgse = new ImageSegmenter(sourcePath, dictPath, useOwnDict);
        imgse.doSegmentation();
    }
    
}
