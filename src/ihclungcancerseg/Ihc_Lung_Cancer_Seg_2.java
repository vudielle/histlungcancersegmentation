/*
 * Copyright 2016, Florian Wassmer, Valeria De Luca, Ruben Casanova
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ihclungcancerseg;

import ij.*;
import ij.gui.YesNoCancelDialog;
import ij.io.DirectoryChooser;
import ij.plugin.filter.PlugInFilter;
import ij.process.*;
import java.awt.*;
import java.io.File;

/**
 *
 * @author Florian Wassmer
 */
public class Ihc_Lung_Cancer_Seg_2 implements PlugInFilter{

    @Override
    public int setup(String string, ImagePlus ip) {
        return DOES_RGB;
    }

    @Override
    public void run(ImageProcessor ip) {
        
        ImagePlus img = WindowManager.getCurrentImage();
        String origName = img.getShortTitle();
        
        DirectoryChooser dir;
        dir = new DirectoryChooser("Choose output folder: ");
        String outputPath = dir.getDirectory();
        if (outputPath == null){
            return;
        }
        
        boolean useOwnDict;
        String msg = "Do you want to use your own visual dictionary?";
        YesNoCancelDialog yncdiag = new YesNoCancelDialog(null, "Use own dictionary?", msg);
        if (yncdiag.cancelPressed()){
            return;
        }
        useOwnDict = yncdiag.yesPressed();
        String dictPath = "";
        if (useOwnDict){
            dir = new DirectoryChooser("Choose Visual Dictionary path: ");
            dictPath = dir.getDirectory();
            if (dictPath == null){
                return;
            }
        }
        
        ImageSegmenter imgse = new ImageSegmenter(ip, outputPath, origName, dictPath, useOwnDict);
        imgse.doSegmentation();
        ImagePlus result = new ImagePlus(outputPath + File.separator + origName + "_ihc_seg.tif");
        result.show();
    }
    
}
